#!/bin/sh
export SECONDS_TO_WAIT_FOR_SERVER_TO_START='180'
export SECONDS_TO_WAIT_FOR_SERVER_TO_STOP='180'
export SECONDS_TO_SLEEP_BETWEEN_CHECKS='3'

export EXT_SSH_FLAGS=" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "

function fail {
   if [ "$#" != 1 ]; then
       echo "Illegal number of parameters: usage: fail <message>"
       exit 1
   fi

   echo "$1" && exit 1
}

# Makes a temporary file copy of the given hosts file template and populates it
# with the alpha and beta external DNS names. The hosts template should contain
# "ALPHA_HOSTNAME", "BETA_HOSTNAME" and a [cluster_hosts] group
#
# [1] alphaAddress
# [2] betaAddress
# [3] hostsTemplateFile - location of the hosts file template
# [4] tempFile - location of the temp file to use
function createModifiedHostsFile {

   if [ "$#" != 4 ]; then
       echo "Illegal number of parameters: Usage: createModifiedHostsFile <ip of alpha> <ip of beta> <hosts template file> <temporary file path to use>"
       exit 1
   fi

    alphaAddress=$1
    betaAddress=$2
    hostsTemplateFile=$3
    tempFile=$4

    echo "$1 $2 $3 $4"
}

# [1] = ipAddress, the ip address of the server to deploy to.
# [2] = remoteJbossHome, the remote jboss_home directory
function stopWildfly {

    if [ "$#" != 2 ]; then
       fail "Illegal number of parameters! Usage: stopWildfly <ipAddress> <remoteJbossHome>"
    fi

    ipAddress=$1
    remoteJbossHome=$2

    echo "stopWildfly $ipAddress $remoteJbossHome"
}

# [1] = ipAddress, the ip address of the server to deploy to.
# [2] = app server local bind address
# [3] = directoryWithEar - the directory with the ear file
# [4] = remoteJbossHome, the remote jboss_home directory
function deploy {
    if [ "$#" != 4 ]; then
       fail "Illegal number of parameters! Usage: deploy <ipAddress> <remote jboss bind address> <ear file dir> <remote jboss home> "
    fi

    ipAddress=$1
    localBindAddress=$2
    earFile="$3/mach_app.ear"
    remoteJbossHome=$4

    echo "deploy $1 $2 $3 $4"
}

# [1] = ipAddress, the ip address of the server to deploy to.
# [2] = app server local bind address
# [3] = directoryWithEar - the directory with the ear file
# [4] = expectedVersion, the expected version to be deployed
# [5] = remoteJbossHome, the remote jboss_home directory
function deployAndVerify {

    if [ "$#" != 5 ]; then
       fail "Illegal number of parameters! Usage: deployAndVerify <ipAddress> <remote jboss bind address> <ear file dir> <expectedVersion> <remote jboss home> "
    fi

    deploy $1 $2 "${3}" "${5}"

    localBindAddress=$2
    expectedVersion=$4

    echo "deployAndVerify $1 $2 $3 $4, $5"
}

function performDeploy {

   if [ "$#" != 2 ]; then
       fail "Illegal number of parameters: usage: performDeploy <ipAddress> <file>"
   fi

   echo "performDeploy $1 $2"
}

# [1] = ipAddress, the ip address of the server to use to tag the deployment in New Relic.
# [2] = tag, the tag for the deployment
function tagDeployment() {
   if [ "$#" != 2 ]; then
       fail "Illegal number of parameters: usage: tagDeployment <ipAddress> <tag>"
   fi

   export ipAddress=$1
   export tag=$2

   echo "tagDeployment $1 $2"
}

# [1] = ipAddress, the ip address of the server to deploy to.
# [2] = the path to the distribution archive
function deployDataImporter() {
   if [ "$#" != 2 ]; then
       fail "Illegal number of parameters: usage: deployDataImporter <ipAddress> <archive>"
   fi

   export ipAddress=$1
   export archive=$2

   echo "deployDataImporter $1 $2"
}
