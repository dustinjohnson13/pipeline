#!groovy

stage('Build') {
    node {
        checkout scm

        if (containsCheckpoint('Build')) {
            echo "Skipping Build"
        } else {
            sh 'env'

            echo "Build Done"

            sh 'mkdir build || true'
            checkpoint('Build')
        }
        stash name: 'built'
    }
}

stage('Release') {

    String emailRecipients = 'development@machenergy.com'
    String slackChannel = '#devops'
    boolean downtimeRequired

    timeout(time: 5, unit: 'DAYS') {
        def downtime = [$class: 'BooleanParameterDefinition', defaultValue: false, description: 'Downtime Required?', name: 'downtime']

        def userInput = input(id: 'userInput', message: 'Perform a release?', parameters: [downtime])

        downtimeRequired = userInput

        echo("Downtime Required: " + downtimeRequired)
    }

    node {
        unstash 'built'

        def commit = commit()
        if (containsCheckpoint('Release Part 1')) {
            echo "Skipping Release Part 1 - $commit"
        } else {
            wrap([$class: 'BuildUser']) {
                echo "Release Part 1 Done - $commit"
                sh '''
                        ##############################################
                        # Perform post-deployment actions and cleanup.
                        ##############################################

                        export TAG=''' + commit + '''

                        echo "Tagging the deployment as [$TAG] in New Relic"
                '''
                checkpoint('Release Part 1')
            }
        }

        if (containsCheckpoint('Release Part 2')) {
            echo "Skipping Release Part 2 - $commit"
        } else {
            sh './gradlew downtimeRelease'

            echo "Release Part 2 Done - $commit"
            checkpoint('Release Part 2')
        }
    }
}

def commit() {
    def commit = sh script: 'git rev-parse HEAD', returnStdout: true
    return commit.trim()
}

def checkpointFilePath() {
    return "build/${commit()}".toString()
}

def currentCheckpoint() {
    def file = checkpointFilePath()

    return fileExists(file) ? readFile(file) : ''
}

def checkpoint(String step) {
    def existingCheckpoints = currentCheckpoint()
    if ('' != existingCheckpoints) {
        existingCheckpoints += ','
    }

    existingCheckpoints += step

    writeFile file: checkpointFilePath(), text: existingCheckpoints
}

def containsCheckpoint(String checkpoint) {
    def completedSteps = currentCheckpoint().split(',')
    for (String completedStep : completedSteps) {
        if (checkpoint == completedStep) {
            return true
        }
    }
    return false
}
